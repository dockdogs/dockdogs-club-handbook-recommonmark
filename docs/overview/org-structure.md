Organizational Structure
=======================

* DockDogs® Worldwide 330-241-4975  
* Grant Reeves CEO -  grant.reeves@dockdogs.com
* Teresa Reeves – General Manager Teresa.reeves@dockdogs.com
* Vicki Tighe – Director of Affiliates vicki.tighe@dockdogs.com 239-464-1859
* Betsy Taylor – S/C-S/W Regional Rep betsy.taylor@dockdogs.com 337-371-8551
* Joan Gunby – N/E Regional Rep joan.gunby@dockdogs.com 410-903-7137 
* Linda Ruiz – N/C – N/W Regional Rep lindaruiz@dockdogs.com 763-234-9538
* Lisa Hudgens- S/E Regional Rep lisa.hudgens@dockdogs.com 865-406-5994   
* Brian King – IT  brian.king@dockdogs.com 
* Theresa Bufilino – Accounting theresa.bufalino@dockdogs.com
* Linda Torson – Administration, Elections dd_admin@dockdogs.com
* Sam Bruno – Accounting sam.bruno@dockdogs.com
* Brian Sharenow – Events brian.sharenow@dockdogs.com
* Sean Swearinger – Event Crew Manager sean.swearinger@dockdogs.com 
